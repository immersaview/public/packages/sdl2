**Nuget packaging for SDL2 Repository**
https://www.libsdl.org/

## Example CMake Usage:
```cmake
target_link_libraries(target SDL2 SDL2main)
target_include_directories(target PRIVATE "$<TARGET_PROPERTY:SDL2,INTERFACE_INCLUDE_DIRECTORIES>")
```
etc.